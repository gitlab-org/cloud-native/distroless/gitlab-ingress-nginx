# Copyright 2015 The Kubernetes Authors. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi:8.9
ARG UBI_IMAGE_MICRO=registry.access.redhat.com/ubi8/ubi-micro:8.9

FROM ${UBI_IMAGE_MICRO} as initial

FROM registry.access.redhat.com/ubi8/ubi:8.9 as builder

COPY . /

RUN /build.ubi.sh

# Use a multi-stage build
FROM registry.access.redhat.com/ubi8/ubi:8.9 as intermediate

ENV PATH=$PATH:/usr/local/luajit/bin:/usr/local/nginx/sbin:/usr/local/nginx/bin:/usr/local/lib:/lib64:/lib

ENV LUA_PATH="/usr/local/share/luajit-2.1.0-beta3/?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/lib/lua/?.lua;;"
ENV LUA_CPATH="/usr/local/lib/lua/?/?.so;/usr/local/lib/lua/?.so;;"
ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS_ROOT="--installroot=${DNF_INSTALL_ROOT}/ --setopt=reposdir=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ --setopt=varsdir=/install-var/ --config=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ubi.repo --setopt=cachedir=/install-cache/ --noplugins --releasever=8.9 -y --nodocs"
ARG DNF_OPTS="--disableplugin=subscription-manager --nodocs --best"

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=initial / ${DNF_INSTALL_ROOT}/

COPY --from=builder /usr/local ${DNF_INSTALL_ROOT}/usr/local
COPY --from=builder /opt/modsecurity ${DNF_INSTALL_ROOT}/opt/modsecurity
COPY --from=builder /etc/nginx ${DNF_INSTALL_ROOT}/etc/nginx

RUN touch ${DNF_INSTALL_ROOT}/etc/ld.so.conf.d/local_libs.conf \
   && echo "/usr/local/lib" >> ${DNF_INSTALL_ROOT}/etc/ld.so.conf.d/local_libs.conf \
   && echo "/usr/local/lib/lua" >> ${DNF_INSTALL_ROOT}/etc/ld.so.conf.d/local_libs.conf \
   && echo "/usr/local/lib/mimalloc-1.6" >> ${DNF_INSTALL_ROOT}/etc/ld.so.conf.d/local_libs.conf \
   && ldconfig -v

ARG TARGETARCH

RUN dnf ${DNF_OPTS} ${DNF_OPTS_ROOT} -y update \
  && dnf ${DNF_OPTS} ${DNF_OPTS_ROOT} -y upgrade \
  && dnf ${DNF_OPTS} ${DNF_OPTS_ROOT} -y install \
    bash \
    pcre \
    zlib \
    curl ca-certificates \
    patch \
    yajl \
    libxml2 \
    libyaml \
    nano \
    openssl \
    tzdata \
  && dnf ${DNF_OPTS} -y install \
    shadow-utils \
    wget \
  && dnf ${DNF_OPTS_ROOT} clean all \
  && rm -rf \
    ${DNF_INSTALL_ROOT}/var/cache/dnf \
  && ARCH=$(echo $TARGETARCH | sed -e s/^amd64$/x86_64/ -e s/^arm64$/aarch64/) \
  && wget -O ${DNF_INSTALL_ROOT}/usr/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.3/dumb-init_1.2.3_${ARCH} \
  && chmod +x ${DNF_INSTALL_ROOT}/usr/bin/dumb-init \
  && ln -s /usr/local/nginx/sbin/nginx ${DNF_INSTALL_ROOT}/sbin/nginx \
  && groupadd -R ${DNF_INSTALL_ROOT}/ -rg 101 www-data \
  && adduser -R ${DNF_INSTALL_ROOT}/ -u 101 -M -d /usr/local/nginx \
     -s /sbin/nologin -G www-data -g www-data www-data

RUN groupadd -rg 101 www-data \
  && adduser -u 101 -M -d /usr/local/nginx \
     -s /sbin/nologin -G www-data -g www-data www-data \
  && bash -eu -c ' \
  writeDirs=( \
    ${DNF_INSTALL_ROOT}/var/log/nginx \
    ${DNF_INSTALL_ROOT}/var/lib/nginx/body \
    ${DNF_INSTALL_ROOT}/var/lib/nginx/fastcgi \
    ${DNF_INSTALL_ROOT}/var/lib/nginx/proxy \
    ${DNF_INSTALL_ROOT}/var/lib/nginx/scgi \
    ${DNF_INSTALL_ROOT}/var/lib/nginx/uwsgi \
    ${DNF_INSTALL_ROOT}/var/log/audit \
  ); \
  for dir in "${writeDirs[@]}"; do \
    mkdir -p ${dir}; \
    chown -R www-data.www-data ${dir}; \
  done'

FROM ${UBI_IMAGE_MICRO}

ENV PATH=$PATH:/usr/local/luajit/bin:/usr/local/nginx/sbin:/usr/local/nginx/bin:/usr/local/lib:/lib64:/lib

ENV LUA_PATH="/usr/local/share/luajit-2.1.0-beta3/?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/lib/lua/?.lua;;"
ENV LUA_CPATH="/usr/local/lib/lua/?/?.so;/usr/local/lib/lua/?.so;;"
ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

ARG DNF_INSTALL_ROOT=/install-root
COPY --from=intermediate  ${DNF_INSTALL_ROOT}/ /

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]