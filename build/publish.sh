#!/usr/bin/env bash

set -euo pipefail

for image_file in artifacts/final/*.txt; do
  image_url="$(cat "${image_file}")"

  # Extract the image repository
  staging_repo="$(skopeo inspect "docker://${image_url}" --format='{{ .Name }}')"

  # Extract the image name from the repo
  image_name="${staging_repo##*/}"

  # Construct the new image repository name
  # shellcheck disable=SC2154
  image_repo="${CI_REGISTRY_IMAGE}/${image_name}"

  # shellcheck disable=SC2154
  if [[ ${TEST_PUBLISH} == true ]]; then
    echo "DRY RUN - Would run: skopeo copy \"docker://${staging_repo}\" \"docker://${image_repo}\""
  else
    skopeo copy "docker://${staging_repo}" "docker://${image_repo}"
  fi
done

